module.exports = (sequelize, Sequelize) => {
    const Cliente = sequelize.define("Clientes", {
        cedula: {
            type: Sequelize.BIGINT,
            unique: true
        },
        nombre_cliente: {
            type: Sequelize.STRING
        },
        apellido: {
            type: Sequelize.STRING
        },
        id_cliente: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    });

    Cliente.associations = models => {
        Cliente.hasMany(models.Reserva, {
            onDelete: "cascade"
        });
    };

    return Cliente;
};