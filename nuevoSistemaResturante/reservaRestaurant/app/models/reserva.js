module.exports = (sequelize, Sequelize) => {
    const Reserva = sequelize.define("Reservas", {
        id_restaurante: {
            type: Sequelize.BIGINT,
            references: {
                model: 'Restaurantes',
                key: 'id_restaurante'
            }
        },
        id_mesa: {
            type: Sequelize.BIGINT,
            references: {
                model: 'Mesas',
                key: 'id_mesa'
            }
        },
        id_cliente: {
            type: Sequelize.BIGINT,
            references: {
                model: 'Clientes',
                key: 'id_cliente'
            }
        },
        fecha: {
            type: Sequelize.DATE
        },
        rango_hora: {
            type: Sequelize.TIME
        },
        cantidad_solicitada: {
            type: Sequelize.INTEGER
        },
        id_reserva: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    });

    Reserva.associations = models => {
        Reserva.belongsTo(models.Restaurant,{
            foreignKey: {
                allowNull: false,
                onDelete: "cascade"
            }
        });
        Reserva.belongsTo(models.Cliente, {
            foreignKey: {
                allowNull: false,
                onDelete: "cascade"
            }
        });
        Reserva.belongsTo(models.Mesa, {
           foreignKey: {
               allowNull: false,
               onDelete: "cascade"
           }
        });
    };

    return Reserva;
};