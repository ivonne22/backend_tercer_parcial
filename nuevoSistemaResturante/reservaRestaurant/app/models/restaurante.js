module.exports = (sequelize, Sequelize) => {
    const Restaurante = sequelize.define("Restaurantes", {
        nombre: {
            type: Sequelize.STRING
        },
        direccion: {
            type: Sequelize.STRING
        },
        id_restaurante: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    });

    Restaurante.associations = models => {
        Restaurante.hasMany(models.Mesa, {
            onDelete: "cascade"
        });
       Restaurante.hasMany(models.Reserva, {
            onDelete: "cascade"
        });
    };

    return Restaurante;
};