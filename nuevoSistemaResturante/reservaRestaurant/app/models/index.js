const dbConfig = require("../config/db.config.js");
const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: false,
    port: dbConfig.PORT,
    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.Restaurant = require("./restaurante.js")(sequelize, Sequelize);
db.Cliente = require("./cliente.js")(sequelize,Sequelize);
db.Mesa = require("./mesa.js")(sequelize,Sequelize);
db.Reserva = require("./reserva.js")(sequelize,Sequelize);
module.exports = db;