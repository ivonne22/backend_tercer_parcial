const restaurante = require('../models/restaurante');
module.exports = (sequelize, Sequelize) => {
    const Mesa = sequelize.define("Mesas", {
        nombre_mesa: {
            type: Sequelize.STRING
        },
        id_restaurante: {
            type: Sequelize.BIGINT,
            references: {
                model: 'Restaurantes',
                key: 'id_restaurante'
            }
        },
        posicion: {
            type: Sequelize.STRING
        },
        planta: {
            type: Sequelize.INTEGER
        },
        capacidad: {
            type: Sequelize.INTEGER
        },
        id_mesa: {
            type: Sequelize.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    });

    Mesa.association = models => {
        Mesa.hasMany(models.Reserva, {
            onDelete: "cascade"
        });
        Mesa.belongsTo(models.Restaurant);
    };

    return Mesa;
};