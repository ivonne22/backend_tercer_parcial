const db = require("../models");
const Cliente = db.Cliente;
//const Op = db.Sequelize.Op;

exports.crear = (req, res) => {
// Validate request
    /*  if (!req.body.factura) {
          res.status(400).send({
              message: "Debe enviar numero de factura!"
          });
          return;
      }
  */
// crea un restaurante
    const cliente = {
        nombre_cliente: req.body.nombre_cliente,
        apellido: req.body.apellido,
        cedula: req.body.cedula
    };

// Guardamos a la base de datos
    Cliente.create(cliente)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear un cliente."
            });
        });
};

exports.listarTodos = (req, res) => {
    Cliente.findAll().then(data => res.send(data));
}

exports.buscar = (req, res) => {
    Cliente.findAll({
        where: {
            id_cliente: req.params.id
        }
    }).then(data => res.send(data));
}

exports.eliminar = (req, res) => {
    Cliente.destroy({
        where: {
            id_cliente: req.params.id
        }
    }).then(() => res.send("Se elimino el cliente exitosamente"));
}

exports.editar = (req, res) => {
    Cliente.update({
        cedula: req.body.cedula,
        nombre_cliente: req.body.nombre_cliente,
        apellido: req.body.apellido
    },
    {
        where: { id_cliente: req.body.id }
    }).then(() => res.send("actualizado exitosamente"));
}