const db = require("../models");
const Reserva = db.Reserva;
//const Op = db.Sequelize.Op;

exports.crear = (req, res) => {
// Validate request
    /*  if (!req.body.factura) {
          res.status(400).send({
              message: "Debe enviar numero de factura!"
          });
          return;
      }
  */
// crea un restaurante
    const reserva = {
        id_restaurante: req.body.id_restaurante,
        id_mesa: re.body.id_mesa,
        fecha: req.body.fecha,
        rango_hora: req.body.rango_hora,
        id_cliente: req.body.id_cliente,
        cantidad_solicitada: req.body.cantidad_solicitada
    };

// Guardamos a la base de datos
    Reserva.create(reserva)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear una reserva."
            });
        });
};


exports.listarTodos = (req, res) => {
    Reserva.findAll().then(data => res.send(data));
}

exports.buscar = (req, res) => {
    Reserva.findAll({
        where: {
            id_reserva: req.params.id
        }
    }).then(data => res.send(data));
}

exports.eliminar = (req, res) => {
    Reserva.destroy({
        where: {
            id_reserva: req.params.id
        }
    }).then(() => res.send("Se elimino la reserva exitosamente"));
}

exports.editar = (req, res) => {
    Reserva.update({
            id_restaurante: req.body.id_restaurante,
            id_mesa: re.body.id_mesa,
            fecha: req.body.fecha,
            rango_hora: req.body.rango_hora,
            id_cliente: req.body.id_cliente,
            cantidad_solicitada: req.body.cantidad_solicitada
        },
        {
            where: { id_reserva: req.body.id }
        }).then(() => res.send("actualizado exitosamente"));
}