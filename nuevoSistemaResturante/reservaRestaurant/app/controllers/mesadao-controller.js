const db = require("../models");
const Mesa = db.Mesa;
//const Op = db.Sequelize.Op;

exports.crear = (req, res) => {
// Validate request
    /*  if (!req.body.factura) m{
          res.status(400).send({
              message: "Debe enviar numero de factura!"
          });
          return;
      }
  */
// crea un restaurante
    const mesa = {
        nombre_mesa: req.body.nombre_mesa,
        id_restaurante: req.body.id_restaurante,
        posicion: req.body.posicion,
        planta: req.body.planta,
        capacidad: req.body.capacidad
    };

// Guardamos a la base de datos
    Mesa.create(mesa)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear una mesa."
            });
        });
};

exports.listarTodos = (req, res) => {
    Mesa.findAll().then(data => res.send(data));
}

exports.buscar = (req, res) => {
    Mesa.findAll({
        where: {
            id_mesa: req.params.id
        }
    }).then(data => res.send(data));
}

exports.eliminar = (req, res) => {
    Mesa.destroy({
        where: {
            id_mesa: req.params.id
        }
    }).then(() => res.send("Se elimino la mesa exitosamente"));
}

exports.editar = (req, res) => {
    Mesa.update({
            nombre_mesa: req.body.nombre_mesa,
            id_restaurante: req.body.id_restaurante,
            posicion: req.body.posicion,
            planta: req.body.planta,
            capacidad: req.body.capacidad
        },
        {
            where: { id_mesa: req.body.id }
        }).then(() => res.send("actualizado exitosamente"));
}