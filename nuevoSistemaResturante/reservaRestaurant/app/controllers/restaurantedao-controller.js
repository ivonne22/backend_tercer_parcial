const db = require("../models");
const Restaurante = db.Restaurant;
//const Op = db.Sequelize.Op;

exports.create = (req, res) => {
// Validate request
  /*  if (!req.body.factura) {
        res.status(400).send({
            message: "Debe enviar numero de factura!"
        });
        return;
    }
*/
// crea un restaurante
    const restaurante = {
        nombre: req.body.nombre,
        direccion: req.body.direccion,
    };

// Guardamos a la base de datos
    Restaurante.create(restaurante)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Ha ocurrido un error al crear un restaurante."
            });
        });
};


exports.listarTodos = (req, res) => {
    Restaurante.findAll().then(data => res.send(data));
}

exports.buscar = (req, res) => {
    Restaurante.findAll({
        where: {
            id_restaurante: req.params.id
        }
    }).then(data => res.send(data));
}

exports.eliminar = (req, res) => {
    Restaurante.destroy({
        where: {
            id_restaurante: req.params.id
        }
    }).then(() => res.send("Se elimino la reserva exitosamente"));
}

exports.editar = (req, res) => {
    Restaurante.update({
            nombre: req.body.nombre,
            direccion: req.body.direccion
        },
        {
            where: { id_restaurante: req.body.id }
        }).then(() => res.send("actualizado exitosamente"));
}