module.exports = app => {
    const reserva = require("../controllers/reservadao-controller.js");
    var router = require("express").Router();
    router.post("/", reserva.crear);
    router.get("/", reserva.listarTodos);
    router.get("/:id", reserva.buscar);
    router.delete("/delete/:id", reserva.eliminar);
    router.put("/update", reserva.editar);
    app.use('/api/reserva', router);
};