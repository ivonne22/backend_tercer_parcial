module.exports = app => {
    const mesa = require("../controllers/mesadao-controller.js");
    var router = require("express").Router();
    router.post("/", mesa.crear);
    router.get("/", mesa.listarTodos);
    router.get("/:id", mesa.buscar);
    router.delete("/delete/:id", mesa.eliminar);
    router.put("/update", mesa.editar);
    app.use('/api/mesa', router);
};