module.exports = app => {
    const restaurante = require("../controllers/restaurantedao-controller.js");
    var router = require("express").Router();
    router.post("/", restaurante.create);
    router.get("/", restaurante.listarTodos);
    router.get("/:id", restaurante.buscar);
    router.delete("/delete/:id", restaurante.eliminar);
    router.put("/update", restaurante.editar);
    app.use('/api/restaurante', router);
};