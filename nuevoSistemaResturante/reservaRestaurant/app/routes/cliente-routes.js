module.exports = app => {
    const cliente = require("../controllers/clientedao-controller.js");
    var router = require("express").Router();
    router.post("/", cliente.crear);
    router.get("/", cliente.listarTodos);
    router.get("/:id", cliente.buscar);
    router.delete("/delete/:id", cliente.eliminar);
    router.put("/update", cliente.editar);
    app.use('/api/cliente', router);
};