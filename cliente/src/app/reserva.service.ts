import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, pipe, Subject, throwError} from "rxjs";
import {Reserva} from "./modelos/Reserva";
import {environment} from "../environments/environment";
import { Cliente } from "./modelos/Cliente"
import { Mesa, Posicion } from "./modelos/Mesa";
import { Restaurante } from "./modelos/Restaurante";
import {catchError, map, mapTo, tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ReservaService {

  private URL_BASE = environment.API_URL;
  private _refreshNeeded$ = new Subject<void>();
  public reservas: Array<{nomRestaurante:string, nomApCliente:string, mesa:number, fecha:string, hora_rango:string, cantPer:number, reservaId:number}> = [];
  public nombreRest: string = '';
  private nomApCliente: string = '';

  constructor(private http: HttpClient) {  }

  getReservas() {
    return this.http.get<Reserva[]>(`${this.URL_BASE}/reserva`)
      .pipe(
        map((reservas)=>{
          this.reservas=[];
          for(let reserva of reservas){
            this.getCliente(reserva.id_cliente).subscribe(value=>{this.nomApCliente = value.nombre_cliente+" "+value.apellido;});
            console.log(reserva.id_restaurante);
            this.getRestaurante(reserva.id_restaurante).subscribe(value => {this.nombreRest = value.nombre
            console.log(value.nombre);});
            this.reservas.push({nomRestaurante:this.nombreRest,nomApCliente:this.nomApCliente,mesa:reserva.id_mesa,fecha:reserva.fecha,hora_rango:reserva.rango_hora,cantPer:reserva.cantidad_solicitada,reservaId:reserva.id_reserva});
          }
          return this.reservas;
        }), catchError(error=>{
          return throwError('Algo salio mal!');
        })
      )
  }

  crearReserva(id_restaurante: number, id_cliente: number, id_mesa: number, cantidad_solicitada: number, fecha: string, rango_hora: string): Observable<Reserva> {
    return this.http.post<Reserva>(`${this.URL_BASE}/reserva`,{id_restaurante, id_cliente, id_mesa, cantidad_solicitada, fecha, rango_hora});
  }

  crearCliente(cedula: number, nombre_cliente: string, apellido: string): Observable<Cliente> {
    console.log("en servicio cedula es: " + cedula);
    return this.http.post<Cliente>(`${this.URL_BASE}/cliente`,{cedula, nombre_cliente, apellido})
  }

  get refreshNeeded$() {
    return this._refreshNeeded$;
  }

  getListaClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(`${this.URL_BASE}/cliente`);
  }

  getBuscarCliente(cedula: number) {
    return this.http.get<Cliente>(`${this.URL_BASE}/cliente/cedula/${cedula}`);
  }
  getMesa(id_mesa: number) {
    return this.http.get<Mesa>(`${this.URL_BASE}/mesa/${id_mesa}`);
  }

  getReservasHechas(id_restaurante: number, fecha: object) {
    return this.http.get<Reserva[]>(`${this.URL_BASE}/mesa/${id_restaurante}/${fecha}`);
  }

  getMesasRestauranteParaReserva(id_restaurante: number, id_mesa: string) {
    return this.http.get<Mesa[]>(`${this.URL_BASE}/mesa/reserva/${id_restaurante}/${id_mesa}`);
  }

  getMesasRestaurante(id_restaurante: number) {
    return this.http.get<Mesa[]>(`${this.URL_BASE}/mesa/${id_restaurante}`);
  }

  getRestaurante(id_restaurante: number):Observable<Restaurante> {
    return this.http.get<Restaurante>(`${this.URL_BASE}/restaurante/${id_restaurante}`);
  }

  getListaRestaurante() {
    return this.http.get<Restaurante[]>(`${this.URL_BASE}/restaurante`);
  }

  getCliente(id_cliente: number):Observable<Cliente> {
    return this.http.get<Cliente>(`${this.URL_BASE}/cliente/${id_cliente}`);
  }
  getReserva(id_reserva: bigint) {
    return this.http.get<Reserva>(`${this.URL_BASE}/reserva/${id_reserva}`);
  }

}
