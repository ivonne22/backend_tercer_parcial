import {Component, OnInit, ViewChild} from '@angular/core';
import { Reserva } from "../modelos/Reserva";
import {ReservaService} from "../reserva.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";


@Component({
  selector: 'app-listar-reserva',
  templateUrl: './listar-reserva.component.html',
  styleUrls: ['./listar-reserva.component.css']
})
export class ListarReservaComponent implements OnInit {

  public loading = true;
  public mensajeError: string = "";
  public mensajeExito: string = "";
  public reservas: Array<{nomRestaurante:string, nomApCliente:string, mesa:number, fecha:string, hora_rango:string, cantPer:number, reservaId:number}> = [];
  // @ts-ignore
  public dataSource: MatTableDataSource<reservas>;
  public columns = ['mesa','restaurante','nombre_cliente','fecha','rango_hora','cantidad_solicitada','id_reserva']

  // @ts-ignore
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private servicioReserva: ReservaService) { }

  ngOnInit(): void {
    this.servicioReserva.getReservas()
      .subscribe(value => {
        this.reservas = value;
        this.loading=false;
    });
  }

  filtro(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ordenar(){
    this.dataSource.sort = this.sort;
  }

}
