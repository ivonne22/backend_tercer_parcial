import { Component,NgModule ,OnInit } from '@angular/core';
import {Restaurante} from "../modelos/Restaurante";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {FormControl, Validators} from "@angular/forms";
import {Mesa, Posicion} from "../modelos/Mesa";
import {ReservaService} from "../reserva.service";
import {Reserva} from "../modelos/Reserva";
import {MatTableDataSource} from "@angular/material/table";
import {Cliente} from "../modelos/Cliente";

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {

  public mensajeError: string = '';
  public mensajeExito: string = '';
  public restaurantes:Restaurante[]=[];
  public posicion: Posicion = {ejeX: 0, ejeY: 0}
  public mesas:Mesa[]=[];
  public reservas: Reserva[]=[];
  public restaurantesControl=new FormControl('',Validators.required)
  public reservasHorarios=new FormControl('',Validators.required)
  public reservasFecha = new FormControl('',Validators.required)
  public clienteControl = new FormControl('',Validators.required)
  public mesaTabla = new FormControl('')
  public mesa: Mesa = {id_mesa: 0, nombre_mesa: '', id_restaurante: 0,  posicion: this.posicion, planta: 1, capacidad: 0};
  public restauranteModel: Restaurante={id_restaurante:0,nombre:'',direccion:''};
  public horarios:string[]=['12:00-13:00','13:00-14:00','14:00-15:00','19:00-20:00','20:00-21:00','22:00-23:00'];
  public nuevaReserva: Reserva = {id_reserva:0,id_restaurante: 0,id_cliente:0,id_mesa:0,cantidad_solicitada:0,rango_hora:"",fecha:""};
  public clienteVacio: Cliente={id_cliente:-1,cedula:-1,nombre_cliente:'',apellido:''};
  public cliente: Cliente=this.clienteVacio;
  public encontrado: boolean=false;
  buscado: boolean=false;

  public loading = true;
  // @ts-ignore
  public dataSource: MatTableDataSource<Mesa>;
  public columns = ['nombre','capacidad','reservar'];

  public cedula: number=0;
  public nombre_cliente: string = '';
  public apellido: string = '';


  constructor(private servicioReserva: ReservaService) {

  }

  ngOnInit(): void {

    this.servicioReserva.getListaRestaurante()
      .subscribe((restaurauntes: Restaurante[]) => {
        this.restaurantes = restaurauntes;
      },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
     }

   numSequence(n: number): Array<number> {
    return Array(n);
  }

  seleccionado(rest: Restaurante, fecha: object, hora: string) {
    this.buscado = true;
    this.servicioReserva.getReservasHechas(rest.id_restaurante,fecha)
  .subscribe((reservas: Reserva[]) => {
    console.log(reservas);
    this.reservas = reservas;
    var mesasReservadas = [];
    hora = hora + '';
    var varHoras = hora.slice(1, hora.length).split(',');
    var i;
    for (i = 0; i < this.reservas.length; i++) {
      if (this.reservas[i].rango_hora.includes(varHoras[0])) {
        mesasReservadas.push(this.reservas[i].id_mesa);
      } else if (this.reservas[i].rango_hora.includes(varHoras[1])) {
        mesasReservadas.push(this.reservas[i].id_mesa);
      }
      console.log(mesasReservadas);
    }
    if (mesasReservadas.length > 0) {
      console.log("Entro en mayor a cero");
      console.log("Valor de id_restaurante es: " + rest.id_restaurante);
      this.servicioReserva.getMesasRestauranteParaReserva(rest.id_restaurante, mesasReservadas.toString())
        .subscribe((mesas: Mesa[]) => {
            this.mesas = mesas;
            console.log("Las mesas para reservar son: " + mesas);
          },
          (error: ErrorEvent) => {
            console.log("entro en error");
            this.mensajeError = error.error.message;
          });
    } else {
      this.servicioReserva.getMesasRestaurante(rest.id_restaurante)
        .subscribe((mesas: Mesa[]) => {
            console.log("Entro en menor a cero")
            this.mesas = mesas;
            console.log(mesas);
          },
          (error: ErrorEvent) => {
            this.mensajeError = error.error.message;
          });
    }
  },
    (error: ErrorEvent) => {
      this.mensajeError = error.error.message;
    });
  }


  anotar(mesa: Mesa,rest: Restaurante,fecha: string, hora: string){
    console.log(mesa.id_mesa);
    this.nuevaReserva.id_restaurante = rest.id_restaurante;
    this.nuevaReserva.fecha = fecha;
    this.nuevaReserva.rango_hora = hora;
    this.nuevaReserva.id_mesa = mesa.id_mesa;
    this.nuevaReserva.cantidad_solicitada = mesa.capacidad;
    this.mesas = [];
    this.mesas.push(mesa);
  }

  buscarCliente(cedula: number){
    this.mensajeError = '';
    this.mensajeExito = '';
    this.servicioReserva.getBuscarCliente(cedula)
      .subscribe((clienteEncontrado: Cliente) => {
        if(clienteEncontrado.cedula != null) {
          this.nombre_cliente = clienteEncontrado.nombre_cliente;
          this.apellido = clienteEncontrado.apellido;
          this.cliente = clienteEncontrado;
          this.encontrado = true;
        }  else {
          this.nombre_cliente = '';
          this.apellido = '';
          this.cliente = this.clienteVacio;
        }
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
  }

  reservar(cedula:number,nombre_cliente:string,apellido:string) {
    this.mensajeExito = '';
    this.mensajeError = '';
    if(!this.encontrado){
      console.log("datos: ",cedula,nombre_cliente,apellido);
      this.servicioReserva.crearCliente(cedula, nombre_cliente, apellido)
        .subscribe((clienteCreado: Cliente) => {
            this.mensajeExito = `Cliente ${this.nombre_cliente} registrado correctamente`;
            this.cedula=0;
            this.nombre_cliente = "";
            this.apellido="";
            this.cliente = clienteCreado;
          },
          (error: ErrorEvent) => {
            this.mensajeError = error.error.message;
          });
    } else{
      this.nuevaReserva.id_cliente = this.cliente.id_cliente;
    }
    this.servicioReserva.crearReserva(this.nuevaReserva.id_restaurante, this.nuevaReserva.id_cliente, this.nuevaReserva.id_mesa, this.nuevaReserva.cantidad_solicitada, this.nuevaReserva.fecha, this.nuevaReserva.rango_hora.toString())
      .subscribe((reservaCreada: Reserva) => {
          this.mensajeExito = `id cliente` + this.nuevaReserva.id_cliente;
          this.cedula=0;
          this.nombre_cliente = "";
          this.apellido="";
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
    this.encontrado = false;

  }

}
