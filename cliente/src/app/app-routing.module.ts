import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HOME} from "@angular/cdk/keycodes";
import {HomeComponent} from "./home/home.component";
import {ListarReservaComponent} from "./listar-reserva/listar-reserva.component";
import {ClientesComponent} from "./clientes/clientes.component";
import {MapaComponent} from "./mapa/mapa.component";
import {ReservaComponent} from "./reserva/reserva.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'lista-reservas',
    component: ListarReservaComponent
  },
  {
    path: 'cliente',
    component: ClientesComponent
  },
  {
    path: 'mapa',
    component: MapaComponent
  },
  {
    path: 'reserva',
    component: ReservaComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
