import {Component, NgModule, OnInit} from '@angular/core';
import {Restaurante} from "../modelos/Restaurante";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {FormControl, Validators} from "@angular/forms";
import {Graf, Mesa} from "../modelos/Mesa";
import {ReservaService} from "../reserva.service";



@Component({
  selector: 'app-restaurantes',
  templateUrl: './mapa.component.html',
  styleUrls: ['./mapa.component.css']
})
export class MapaComponent implements OnInit {

  public mensajeError: string = '';
  public mensajeExito: string = '';
  restaurantes:Restaurante[]=[];
  mesas:Mesa[]=[];
  columnasStr:string[]=[];
  filas:number[]=[];
  restaurantesControl=new FormControl('',Validators.required)
  plantasControl=new FormControl();
  restaurantesHorarios=new FormControl('',Validators.required)
  restauranteModel: Restaurante={id_restaurante:0,nombre:'',direccion:''};
  plantaModel:number=0;
  horarios:string[]=['12:00-13:00','13:00-14:00','14:00-15:00','19:00-20:00','20:00-21:00','22:00-23:00'];
  tamano:number[]=[1,2,3,4];
  mesaVacia: Mesa= {id_mesa:-1, nombre_mesa:'', id_restaurante:-1, planta:-1, posicion:{ ejeX: -1, ejeY: -1}, capacidad:0};
  matriz: Mesa[][]=[ [this.mesaVacia,this.mesaVacia,this.mesaVacia, this.mesaVacia, this.mesaVacia],
                      [this.mesaVacia,this.mesaVacia,this.mesaVacia, this.mesaVacia, this.mesaVacia],
                      [this.mesaVacia,this.mesaVacia,this.mesaVacia, this.mesaVacia, this.mesaVacia],
                      [this.mesaVacia,this.mesaVacia,this.mesaVacia, this.mesaVacia, this.mesaVacia]];
  marcado: boolean = false;
  id_rest: number=-1;
  planta: number=0;
  plantaMasAlta: number=-1;
  plantas: number[]=[];
  nuevo: Array<Graf>=[];
  restauranteElegido: number=-1;


  constructor(private servicioReserva: ReservaService) {
  }

  ngOnInit(): void {
    this.servicioReserva.getListaRestaurante()
      .subscribe((restaurauntes: Restaurante[]) => {
          this.restaurantes = restaurauntes;
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
  }

  numSequence(n: number): Array<number> {
    return Array(n);
  }

  cargarPlantas(idRestaurante:number) {
    this.plantas = [];
    this.plantaMasAlta = 0;

    if (idRestaurante != undefined)
      this.restauranteElegido = idRestaurante;
    else
      this.restauranteElegido = this.restaurantes[0].id_restaurante;

    this.servicioReserva.getMesasRestaurante(this.restauranteElegido)
      .subscribe((mesas: Mesa[]) => {
          this.mesas = mesas;
          for (let i of this.mesas) {
            var aux = i.posicion.toString().split("(",2)[1].toString();
            i.posicion = {ejeX:0,ejeY:0};
            aux = aux.split(")",2)[0].toString();
            var coordenadas = aux.split(",",2);
            i.posicion.ejeX = +coordenadas[0];
            i.posicion.ejeY = +coordenadas[1];
          }
          for (let i of this.mesas) {
            if (i.planta > this.plantaMasAlta)
            this.plantaMasAlta = i.planta;
          }
          for (let i = 1; i <= this.plantaMasAlta; i++)
            this.plantas.push(i);

        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
  }

  searchMesas(idRest:number,planta:number):Array<Mesa>{
    this.planta = planta;
    this.marcado = true;
    var columnas:number[]=[];
    this.filas=[];
    var mesa:Array<Mesa>=[];
    console.log(this.mesas);
    for(let i of this.mesas) {
      console.log("entro en el for");
      console.log("el valor de i.planta es: " + i.planta + " el valor de planta es: " + planta);
      if (i.planta == planta) {
        console.log("esta pusheando");
        mesa.push(i);
      }
    }
    console.log("entro en search mesa y las mesas son: ");
    console.log(mesa)
    for(let i of mesa) {
      if(!columnas.includes(i.posicion.ejeY)){
        console.log("lo que se ve a agregar es: " + i.posicion);
        columnas.push(i.posicion.ejeY);
      }
    }
    console.log("columnas tiene: ");
    console.log(columnas);
    for(let i of mesa) {
      if(!this.filas.includes(i.posicion.ejeX)){
        this.filas.push(i.posicion.ejeX);
      }
    }
    mesa.sort(function (a,b){
      if(a.posicion.ejeY == b.posicion.ejeY){
       return a.posicion.ejeX-b.posicion.ejeX
      }
      return a.posicion.ejeY-b.posicion.ejeY;
    })
    this.filas.sort(function (a,b) {
      return a-b;
    })
    columnas.sort(function (a,b) {
      return a-b;
    })
    var aux:Array<Mesa>=[];
    for(let value of columnas) {
      var column=value
      for(let valueY of mesa) {
        if(value == valueY.posicion.ejeY){
          aux.push(valueY);
        }
      }
      // @ts-ignore
      var messas:Graf={col:column,mesas:aux}
      this.nuevo.push(messas);
      aux=[];
      this.columnasStr.push(value.toString())
    }

    /*this.filas.forEach(valueX => {
      nuevo.push(valueY.posicion.ejeY,)
    })*/

    for(var fila=0;fila<4;fila++)
      for(var col=0;col<5;col++)
        this.matriz[fila][col] = this.mesaVacia;

    for(var mesaAux of this.mesas){
      if(mesaAux.planta == this.planta) {
        this.matriz[mesaAux.posicion.ejeX][mesaAux.posicion.ejeY] = mesaAux;
      }
    }
    return mesa;
  }

}
