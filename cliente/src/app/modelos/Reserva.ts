export interface Reserva {
  id_reserva: number;
  id_restaurante: number;
  id_mesa: number;
  id_cliente: number;
  fecha: string;
  rango_hora: string;
  cantidad_solicitada: number;
}
