export interface Cliente {
  id_cliente: number;
  cedula: number;
  nombre_cliente: string;
  apellido: string;
}
