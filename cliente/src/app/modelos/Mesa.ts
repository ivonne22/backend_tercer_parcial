export interface Mesa {
  id_mesa: number;
  nombre_mesa: string;
  id_restaurante: number;
  posicion: Posicion;
  planta: number;
  capacidad: number;
}

export interface Posicion {
  ejeX: number;
  ejeY: number;
}

export interface Graf{
  col: number;
  mesas: Array<Mesa>;
}
