export interface Restaurante {
  id_restaurante: number;
  nombre: string;
  direccion: string;
}
