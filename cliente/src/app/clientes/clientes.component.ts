import { Component, OnInit } from '@angular/core';
import { ReservaService } from '../reserva.service';
import { Cliente } from '../modelos/Cliente'
import * as $ from 'jquery';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  public mensajeExito: string = '';
  public mensajeError: string = '';
  public cedula: number = -1;
  public nombre_cliente: string = '';
  public apellido: string = '';

  constructor(private servicioReserva: ReservaService) { }

  ngOnInit(): void {

  }

  crearCliente(){
    this.mensajeExito = '';
    this.mensajeError = '';
    this.servicioReserva.crearCliente(this.cedula, this.nombre_cliente, this.apellido)
      .subscribe((clienteCreado: Cliente) => {
        this.mensajeExito = `Cliente ${this.nombre_cliente} registrado correctamente`;
        this.cedula=0;
        this.nombre_cliente = "";
        this.apellido="";
      },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
  }

  buscarCliente(cedula: number){
    this.mensajeError = '';
    this.mensajeExito = '';
    this.servicioReserva.getBuscarCliente(cedula)
      .subscribe((clienteEncontrado: Cliente) => {
      this.nombre_cliente = clienteEncontrado.nombre_cliente;
      this.apellido = clienteEncontrado.apellido;
    },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
      });
  }

}
